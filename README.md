## 测试数据源联通性
dbt debug

## 安装依赖包
dbt deps

## 执行命令 (执行所有模型)
dbt run

## 执行命令,选择模型执行
dbt run --select path/xxx.sql

## 创建外部表
dbt run-operation stage_external_sources

## 创建外部表使用变量
dbt run-operation stage_external_sources --vars '{"ext_full_refresh": true,"url": "jdbc:mysql://ip:3306/mall-demo", "user": "root","dbtable": "customer","password": "123456"}' --args "select: default.my_mysql_test"

## 执行所有测试（基于schema的定义）
dbt test

## 执行xx模型的测试（基于schema的定义）
dbt test --select path/xxx.sql

## 查看表数据
dbt run-operation preview --args 'sql: select * from [scheme].[tableName]'

## 删除schema命令，谨慎使用
dbt run-operation drop_all_schemas --args 'schema_name: [scheme]'